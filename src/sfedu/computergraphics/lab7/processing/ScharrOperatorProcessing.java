package sfedu.computergraphics.lab7.processing;

public class ScharrOperatorProcessing extends DifferentialOperatorProcessing {

    public ScharrOperatorProcessing() {
        xMask = new double[][]
                {
                        { -3, 0,  +3},
                        {-10, 0, +10},
                        { -3, 0,  +3},
                };
        yMask = new double[][]
                {
                        {-3, -10, -3},
                        { 0,  0,   0},
                        {+3, +10, +3},
                };
    }

    @Override
    public String getName() {
        return "Оператор Щарра";
    }
}
