package sfedu.computergraphics.lab7.processing;

import javafx.scene.image.Image;
import sfedu.computergraphics.lab7.Pickable;
import sfedu.computergraphics.lab7.coloring.ColoringMethod;

public interface ProcessingMethod extends Pickable {

    Image process(Image sourceImage, ColoringMethod colorant);

}
