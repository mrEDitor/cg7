package sfedu.computergraphics.lab7.processing;

import javafx.scene.image.Image;
import sfedu.computergraphics.lab7.coloring.ColoringMethod;

public class NoOperatorProcessing implements ProcessingMethod {

    @Override
    public Image process(final Image sourceImage, final ColoringMethod colorant) {
        return sourceImage;
    }

    @Override
    public String getName() {
        return "Без обработки";
    }
}
