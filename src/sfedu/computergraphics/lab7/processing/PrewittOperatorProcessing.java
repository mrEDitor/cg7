package sfedu.computergraphics.lab7.processing;

public class PrewittOperatorProcessing extends DifferentialOperatorProcessing {

    public PrewittOperatorProcessing() {
        xMask = new double[][]
                {
                        {-1, 0, +1},
                        {-1, 0, +1},
                        {-1, 0, +1},
                };
        yMask = new double[][]
                {
                        {-1, -1, -1},
                        { 0,  0,  0},
                        {+1, +1, +1},
                };
    }

    @Override
    public String getName() {
        return "Оператор Прюитт";
    }
}
