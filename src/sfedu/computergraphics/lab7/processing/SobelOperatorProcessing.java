package sfedu.computergraphics.lab7.processing;

public class SobelOperatorProcessing extends DifferentialOperatorProcessing {

    public SobelOperatorProcessing() {
        xMask = new double[][]
                {
                        {-1, 0, +1},
                        {-2, 0, +2},
                        {-1, 0, +1},
                };
        yMask = new double[][]
                {
                        {-1, -2, -1},
                        { 0,  0,  0},
                        {+1, +2, +1},
                };
    }

    @Override
    public String getName() {
        return "Оператор Собела";
    }
}
