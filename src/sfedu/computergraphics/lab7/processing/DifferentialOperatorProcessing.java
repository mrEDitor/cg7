package sfedu.computergraphics.lab7.processing;

import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import sfedu.computergraphics.lab7.coloring.ColoringMethod;

public abstract class DifferentialOperatorProcessing implements ProcessingMethod {

    private static final int RED_COMPONENT_OFFSET = 0;
    private static final int GREEN_COMPONENT_OFFSET = 8;
    private static final int BLUE_COMPONENT_OFFSET = 16;
    private static final int COMPONENT_MASK = 0xFF;

    double[][] xMask;
    double[][] yMask;

    public WritableImage process(final Image sourceImage, final ColoringMethod colorant) {
        final int width = (int) sourceImage.getWidth();
        final int height = (int) sourceImage.getHeight();
        final WritableImage processedImage = new WritableImage(width, height);
        final PixelReader reader = sourceImage.getPixelReader();
        final PixelWriter writer = processedImage.getPixelWriter();
        final double operatorXMask[][] = xMask;
        final double operatorYMask[][] = yMask;
        for (int y = 1; y < height - 1; y++) {
            for (int x = 1; x < width - 1; x++) {
                int redX = 0, redY = 0;
                int greenX = 0, greenY = 0;
                int blueX = 0, blueY = 0;
                for (int deltaX = -1; deltaX <= 1; deltaX++) {
                    for (int deltaY = -1; deltaY <= 1; deltaY++) {
                        final int color = reader.getArgb(x + deltaX, y + deltaY);
                        final int red = (color >> RED_COMPONENT_OFFSET) & COMPONENT_MASK;
                        final int green = (color >> GREEN_COMPONENT_OFFSET) & COMPONENT_MASK;
                        final int blue = (color >> BLUE_COMPONENT_OFFSET) & COMPONENT_MASK;
                        redX += red * operatorXMask[deltaX + 1][deltaY + 1];
                        redY += red * operatorYMask[deltaX + 1][deltaY + 1];
                        greenX += green * operatorXMask[deltaX + 1][deltaY + 1];
                        greenY += green * operatorYMask[deltaX + 1][deltaY + 1];
                        blueX += blue * operatorXMask[deltaX + 1][deltaY + 1];
                        blueY += blue * operatorYMask[deltaX + 1][deltaY + 1];
                    }
                }
                final int red = Math.min(Math.abs(redX) + Math.abs(redY), 255);
                final int green = Math.min(Math.abs(greenX) + Math.abs(greenY), 255);
                final int blue = Math.min(Math.abs(blueX) + Math.abs(blueY), 255);
                writer.setColor(x, y, colorant.rgb(red, green, blue));
            }
        }
        return processedImage;
    }
}
