package sfedu.computergraphics.lab7.coloring;

import javafx.scene.paint.Color;
import sfedu.computergraphics.lab7.Pickable;

public interface ColoringMethod extends Pickable {

    Color rgb(int red, int green, int blue);

}
