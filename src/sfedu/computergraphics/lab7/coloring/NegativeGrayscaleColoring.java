package sfedu.computergraphics.lab7.coloring;

import javafx.scene.paint.Color;

public class NegativeGrayscaleColoring implements ColoringMethod {

    @Override
    public String getName() {
        return "Обратные черно-белые границы";
    }

    @Override
    public Color rgb(int red, int green, int blue) {
        return Color.grayRgb(255 - (red / 4 + green / 2 + blue / 4));
    }
}
