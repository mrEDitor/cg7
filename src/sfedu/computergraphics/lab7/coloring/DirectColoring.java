package sfedu.computergraphics.lab7.coloring;

import javafx.scene.paint.Color;

public class DirectColoring implements ColoringMethod {

    @Override
    public String getName() {
        return "Цветные границы";
    }

    @Override
    public Color rgb(int red, int green, int blue) {
        return Color.rgb(red, green, blue);
    }
}
