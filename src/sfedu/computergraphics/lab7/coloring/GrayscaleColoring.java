package sfedu.computergraphics.lab7.coloring;

import javafx.scene.paint.Color;

public class GrayscaleColoring implements ColoringMethod {

    @Override
    public String getName() {
        return "Черно-белые границы";
    }

    @Override
    public Color rgb(int red, int green, int blue) {
        return Color.grayRgb(red / 4 + green / 2 + blue / 4);
    }
}
