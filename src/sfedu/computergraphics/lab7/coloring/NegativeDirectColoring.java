package sfedu.computergraphics.lab7.coloring;

import javafx.scene.paint.Color;

public class NegativeDirectColoring implements ColoringMethod {

    @Override
    public String getName() {
        return "Обратные цветные границы";
    }

    @Override
    public Color rgb(int red, int green, int blue) {
        return Color.rgb(255 - red, 255 - green, 255 - blue);
    }
}
