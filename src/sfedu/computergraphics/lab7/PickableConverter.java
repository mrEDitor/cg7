package sfedu.computergraphics.lab7;

import javafx.util.StringConverter;

class PickableConverter<T extends Pickable> extends StringConverter<T> {

    @Override
    public String toString(T pickable) {
        return pickable.getName();
    }

    @Override
    public T fromString(String string) {
        return null;
    }
}
