package sfedu.computergraphics.lab7;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.GridPane;
import javafx.stage.*;
import javafx.stage.Window;
import sfedu.computergraphics.lab7.coloring.*;
import sfedu.computergraphics.lab7.processing.*;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    public GridPane grid;
    public Button save;
    public ChoiceBox<ProcessingMethod> processing;
    public ChoiceBox<ColoringMethod> coloring;
    public ImageView preview;

    private WritableImage sourceImage;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        processing.setConverter(new PickableConverter<>());
        coloring.setConverter(new PickableConverter<>());
        processing.setItems(getAvailableOperators());
        coloring.setItems(getAvailableColorants());
    }

    private ObservableList<ProcessingMethod> getAvailableOperators() {
        return FXCollections.observableArrayList(
                new NoOperatorProcessing(),
                new PrewittOperatorProcessing(),
                new SobelOperatorProcessing(),
                new ScharrOperatorProcessing()
        );
    }

    private ObservableList<ColoringMethod> getAvailableColorants() {
        return FXCollections.observableArrayList(
                new DirectColoring(),
                new NegativeDirectColoring(),
                new GrayscaleColoring(),
                new NegativeGrayscaleColoring()
        );
    }

    public void open(final ActionEvent event) {
        final Window window = grid.getScene().getWindow();
        final FileChooser chooser = new FileChooser();
        chooser.setTitle("Выберите изображение");
        final File file = chooser.showOpenDialog(window);
        if (file != null) {
            final Stage waitDialog = showWaitingDialog();
            try {
                final BufferedImage awtImage = ImageIO.read(file);
                if (awtImage == null) {
                    throw new IOException("Невозможно декодировать изображение");
                }
                sourceImage = SwingFXUtils.toFXImage(awtImage, null);
                preview.setImage(sourceImage);
                processing.getSelectionModel().select(0);
                coloring.getSelectionModel().select(0);
                processing.setDisable(false);
                coloring.setDisable(false);
                save.setDisable(false);
            } catch (final Exception e) {
                alert(e);
            }
            waitDialog.close();
        }
    }

    public void save(final ActionEvent event) {
        final Window window = grid.getScene().getWindow();
        final FileChooser chooser = new FileChooser();
        chooser.setTitle("Сохранение в файл");
        final File file = chooser.showSaveDialog(window);
        if (file != null) {
            final Stage waitDialog = showWaitingDialog();
            try {
                final Image fxImage = preview.getImage();
                final BufferedImage awtImage = SwingFXUtils.fromFXImage(fxImage, null);
                final int width = awtImage.getWidth();
                final int height = awtImage.getHeight();
                final BufferedImage outImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
                Graphics2D g = outImage.createGraphics();
                g.drawImage(awtImage, 0, 0, width, height, null);
                g.dispose();
                ImageIO.write(outImage, getFileExtension(file), file);
            } catch (final Exception e) {
                alert(e);
            }
            waitDialog.close();
        }
    }

    public void process(final ActionEvent event) {
        final Stage waitDialog = showWaitingDialog();
        final ProcessingMethod diffOperator = processing.getSelectionModel().getSelectedItem();
        final ColoringMethod colorant = coloring.getSelectionModel().getSelectedItem();
        final Image processedImage = diffOperator.process(sourceImage, colorant);
        preview.setImage(processedImage);
        waitDialog.close();
    }

    private String getFileExtension(final File file) throws IOException {
        final String name = file.getName();
        final int lastExtensionOffset = name.lastIndexOf('.');
        if (lastExtensionOffset > 0) {
            return name.substring(lastExtensionOffset + 1);
        } else {
            throw new IOException("Пожалуйста, укажите расширение файла");
        }
    }

    private void alert(final Exception e) {
        final Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Ошибка!");
        alert.setHeaderText("Ошибка!");
        alert.setContentText(e.getLocalizedMessage());
        alert.showAndWait();
    }

    private Stage showWaitingDialog() {
        final Window window = grid.getScene().getWindow();
        final Stage dialog = new Stage(StageStyle.UNDECORATED);
        dialog.initOwner(window);
        dialog.initModality(Modality.APPLICATION_MODAL);
        try {
            final URL resource = getClass().getResource("waiting.fxml");
            final Parent root = FXMLLoader.load(resource);
            final Scene scene = new Scene(root);
            dialog.setScene(scene);
        } catch (final IOException e) {
            alert(e);
        }
        dialog.show();
        return dialog;
    }
}
