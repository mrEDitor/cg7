package sfedu.computergraphics.lab7;

public interface Pickable {

    String getName();

}
